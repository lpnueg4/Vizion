# coding: u8

#=======================================
#         for data.panzura.com
#=======================================

import importlib
import inspect
import os
import re
import requests
from selenium import webdriver
import sys
from texttable import Texttable
import time
import traceback
import urllib

#---------------------------------------------------------------------------------

try:
    sys.path.insert(0, "../../")
except Exception as e:
    pass

from Vizion.log import zlogger, log_to_file
from Vizion.log import robot_logger

logger = zlogger

# API
from Vizion.API.Auth.auth_base import API_auth
from Vizion.API.Tenantsvr.tenantsvr_base import API_tenantsvr

# PAGE
from Vizion.PAGE.Header.header_base import Header_in_page
from Vizion.PAGE.Login.login_base import Login_Page
from Vizion.PAGE.Marketplace.marketplace_base import Marketplace_Page
from Vizion.PAGE.ASE.ase_base import ASE_page

#---------------------------------------------------------------------------------

test_version = "1.8.7"

auth_version = "base1"

def init_module():
    auth_moudle = importlib.import_module("Vizion.API.Auth.auth_%s" % auth_version)

def showv():
    print "heheda, ", test_version
    logger.info(test_version)

def compress_png(file_path):
    resize_rate = 0.6
    resize_rate = 1
    try:
        from PIL import Image
        img = Image.open(file_path)
        w,h = img.size
        w,h = int(round(w * resize_rate)), int(round(h * resize_rate))
        img = img.resize((w,h), Image.ANTIALIAS)
        img.save(file_path, optimize=True, quality=90)

        exe_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "pngquant.exe")
        if not os.path.exists(exe_path):
            logger.error("compress_png, pngquant.exe not exist\n\tplease check path %s" % exe_path)
            raise Exception()

        cmd = "%s --ext .png -f %s" % (exe_path, file_path)
        ret = os.system(cmd)
        if ret != 0:
            logger.error("compress_png, failed")
    except Exception as e:
        logger.error(e)
        logger.error("compress_png, failed")
        logger.critical("\n" + traceback.format_exc())

def check_screen_resolution():
    try:
        from win32api import GetSystemMetrics
        from win32con import SM_CMONITORS, SM_CXVIRTUALSCREEN, SM_CYVIRTUALSCREEN

        logger.info("Here is %s monitor, main monitor size is (%s x %s)" % (GetSystemMetrics(SM_CMONITORS), GetSystemMetrics(0), GetSystemMetrics(1)))
    except Exception as e:
        logger.error(e)
        logger.error("check_screen_resolution, failed")


class Tenant(object):

    def __init__(self, vizion, email=None, password=None):

        self.vizion = vizion

        self.email = email
        self.password = password

        self.id = None
        self.name = None
        self.company = None

        #
        self._exist = None

        if self.exist:
            logger.info("Tenant::__init__, '%s' exist is %s" % (self.email, self.exist))

            # TODO, 这个是在 log.html 这里显示图片测试，可用，但是需要设置好相关路径。
            # 写个show picture的方法，不是robot，就不打印。
            # 后面可以再详细设置下log等级，一般没出错就不显示那么详细。 通过 --loglevel DEBUG:INFO。测试运行的日志级别是 DEBUG, 但是默认的可视级别是 INFO.
            # logger.debug(r'<img src="D:\git\PZA-AF-NEW\libs\Vizion\ENV\get_stacks_info_1.png" width="90%" border="1"/>', html=True)
        else:
            logger.error("Tenant::__init__, '%s' exist is %s" % (self.email, self.exist))
            raise Exception()

        self.show()

    @property
    def exist(self):
        if self._exist == None:
            ret = self.vizion.api_auth.api_tenants_get({'q': self.email})
            if len(ret) == 0:
                self._exist = False
            else:
                for k,v in ret[0].iteritems():
                    setattr(self, k, v)
                self._exist = True

        return self._exist

    def show(self):
        table = Texttable()
        table.set_cols_width([40,20,15,15,8,8,25])

        table.add_row(['Email', 'password', 'name', 'company', 'id', 'status', 'created_at'])
        table.add_row([self.email, self.password, self.name, self.company, self.id, self.status, self.created_at])

        logger.info("\n" + table.draw())


class Elastic_Stack(list):

    def __init__(self):
        pass

    def add(self, name, version, retention_period, kibana_url, kibana_username, kibana_password, kibana_api_url):
        if all([kibana_url, kibana_username, kibana_password, kibana_api_url]):
            data = {
                'name': name,
                'version': version,
                'retention_period': retention_period,
                'kibana_url': kibana_url,
                'kibana_username': kibana_username,
                'kibana_password': kibana_password,
                'kibana_api_url': kibana_api_url,
            }
            self.append(data)
        else:
            logger.error("Elastic_Stack::add, invalid parameters")
            logger.error("\nkibana_url: %s\nkibana_username: %s\nkibana_password: %s\nkibana_api_url: %s" % \
                (kibana_url, kibana_username, kibana_password, kibana_api_url))

    def show(self):
        table = Texttable()
        table.set_cols_width([25,8,10,40,20,20,70])
        table.add_row(['Stack Name', 'Version', 'Retention period', 'Kibana', 'Username', 'Password', 'Elasticsearch API Endpoint'])
        for i in self:
            table.add_row([i['name'], i['version'], i['retention_period'], i['kibana_url'], i['kibana_username'], i['kibana_password'], i['kibana_api_url']])

        logger.info("\n" + table.draw())

#
class Vizion(object):

    def __init__(self, host="app5.vizion.ai", robot=False, work_space="c:/tmp"):
        '''
            work_space: 存放图片的路径
        '''
        self.host = host
        self.robot = robot
        self.work_space = work_space
        self.timestamp = str(int(time.time()))
        self.logger = self.init_logger()
        global logger
        logger = self.logger

        self.c3_email = "c3admin@panzura.com"
        self.c3_pw = "Pan1011Viz!ei"

        if not host:
            logger.error("Vizion::__init__, host is none")
            raise Exception()
        else:
            logger.info("Vizion::__init__, done\n\thost: %s\n\twork space: %s" % (self.host, self.work_space))

        check_screen_resolution()

    def init_module(self):
        auth_moudle = importlib.import_module("Vizion.API.Auth.auth_%s" % auth_version)

    def init_logger(self):
        logger = None
        if self.robot:
            logger = robot_logger()
        else:
            logger = zlogger

        return logger

    def screenshot(self):

        # TODO，可以利用 inspect.stack() 的层数来加tab。让log展示的更清楚。

        try:
            # in jenkins, it is work_space
            base_folder = self.work_space
            if not os.path.exists(self.work_space):
                    os.mkdir(self.work_space)

            file_name = "%s#%s_%s#%s.png" % (os.path.basename(inspect.stack()[1][1]), inspect.stack()[1][3], inspect.stack()[1][2], time.strftime("%m-%d_%H.%M", time.localtime()))
            file_path = os.path.join(base_folder, file_name)
            file_path_src = os.path.join(base_folder, urllib.quote(file_name))
            self.webdriver.save_screenshot(file_path)
            compress_png(file_path)
            if self.robot:
                if re.search(r'jenkins.workspace', base_folder):
                    # 这如果使用jenkins，需要处理一下路径。就使用单纯的文件名。
                    logger.error('<img src="%s" border="1"/>' % urllib.quote(file_name), html=True)
                else:
                    # logger.error('<img src="%s" width="90%%" border="1"/>' % file_path_src, html=True)
                    logger.error('<img src="%s" border="1"/>' % file_path_src, html=True)

        except Exception as e:
            logger.error(e)
            logger.critical("\n" + traceback.format_exc())

    #-----------------------------------------------------------------------
    @property
    def tenant(self):
        return self._tenant

    @tenant.setter
    def tenant(self, value):
        email, password = value
        self._tenant = Tenant(self, email, password)

    #-----------------------------------------------------------------------
    @property
    def api_auth(self):
        if hasattr(self, '_api_auth') and isinstance(self._api_auth, API_auth):
            pass
        else:
            self._api_auth = API_auth(self)

        return self._api_auth

    @api_auth.setter
    def api_auth(self):
        pass

    #-----------------------------------------------------------------------
    @property
    def api_tenantsvr(self):
        if hasattr(self, '_api_tenantsvr') and isinstance(self._api_tenantsvr, API_tenantsvr):
            pass
        else:
            self._api_tenantsvr = API_tenantsvr(self)

        return self._api_tenantsvr

    @api_tenantsvr.setter
    def api_tenantsvr(self):
        pass

    #-----------------------------------------------------------------------
    '''
        关于分辨率。
        想起来了，slave重启以后，需要用高分辨率的屏幕登录一下，不然截图就是小图。甚至导致元素找不到。
        set_window_size，设置大分辨率也没有，就大了一点点。
        maximize_window，max后，就一定是最后关闭时的分辨率。
        办法就只有，使用slave登录slave，来激活一个分辨率，这样比较自动化。
    '''
    @property
    def webdriver(self):
        if hasattr(self, '_webdriver') and self._webdriver.service.process != None:
            pass
        else:
            self._webdriver = webdriver.Chrome()
            self.webdriver.maximize_window()
            logger.info("Vizion::webdriver, init done, %s" % self._webdriver.session_id)
        return self._webdriver

    @webdriver.setter
    def webdriver(self):
        pass

    #-----------------------------------------------------------------------

    @property
    def elastic_stack(self):
        if hasattr(self, '_elastic_stack') and isinstance(self._elastic_stack, list):
            pass
        else:
            self._elastic_stack = Elastic_Stack()
        return self._elastic_stack

    @elastic_stack.setter
    def elastic_stack(self):
        pass

    #-----------------------------------------------------------------------

    def re_login(self):
        logger.info("-"*120)
        logger.info("Vizion::re_login, start")
        try:
            if hasattr(self, '_webdriver') and self._webdriver.service.process != None:
                self._webdriver.quit()

            login_page = Login_Page(self)
            login_page.login()

            self.window_verify_email_flag = False

        except Exception as e:
            logger.error(e)
            logger.error("Vizion::re_login, failed")
            raise Exception()

        logger.info("Vizion::re_login, done")

    def _generate_random_email(self, base_email='pandas@panzura.com'):
        try:
            g = re.search(r'(.*?)(@.*)', base_email)
            if g:
                name = g.group(1)
                domain = g.group(2)
                new_email = "%s+%s%s" % (name, self.timestamp, domain)

                return new_email
        except Exception as e:
            logger.error(e)

    def _generate_random_elk_name(self, base_name='auto_test_elk'):
        try:
            new_name = "%s_%s" % (base_name, self.timestamp)
            return new_name
        except Exception as e:
            logger.error(e)

    def create_tenant(self, email=None, password="QWER123!@#123abc"):
        login_page = Login_Page(self)

        if email == None:
            email = self._generate_random_email()

        login_page.create_new_one(email, password)

        time.sleep(10)
        self.tenant = (email, password)
        logger.info("Vizion::create_tenant, done")

        self.webdriver.quit()

    def enable_ASE(self):
        logger.info("Vizion::enable_ASE, start")

        # use api
        original_data = self.api_tenantsvr.sys_tenant_get(self.tenant.name)
        original_data['ALLOWED_SERVICES'] = "ASF,ASE"

        ret = self.api_tenantsvr.sys_tenant_put(self.tenant.name, original_data)
        if ret['message'].lower() == 'success':
            logger.info("Vizion::enable_ASE, done")
        else:
            logger.error("Vizion::enable_ASE, failed")

    def create_ASE(self, name=None, version='6.5.4', retention_period='Forever'):
        logger.info("Vizion::create_ASE, start")

        if name == None:
            name = self._generate_random_elk_name()

        try:
            self.re_login()

            header = Header_in_page(self)
            header.go_to_marketplace()

            marketplace = Marketplace_Page(self)
            marketplace.add_ASE_services()

            ase_page = ASE_page(self)
            ase_page.generate_stack(name, version, retention_period)

            self.elastic_stack.show()

        except Exception as e:
            logger.error(e)
            logger.critical("\n" + traceback.format_exc())
            logger.error("Vizion::create_ASE, failed")
            raise Exception()

        logger.info("Vizion::create_ASE, done")

    def get_ASE_stacks_info(self, app_id=None):
        '''
            app_id is kibane_username
        '''
        logger.info("Vizion::get_ASE_stacks_info, start")

        try:
            self.re_login() # 必须先relogin，打开页面

            ase_page = ASE_page(self)
            data = ase_page.get_stacks_info(app_id)
        except Exception as e:
            logger.error(e)
            logger.critical("\n" + traceback.format_exc())
            logger.error("Vizion::get_ASE_stacks_info, failed")
            raise Exception()

        logger.info("Vizion::get_ASE_stacks_info, done")

        return data

    def test_log(self):
        try:
            logger.info("this is info")
            logger.debug("this is debug")
            logger.error("this is error")
            logger.warning("this is warning")
            logger.critical("this is critical")
        except Exception as e:
            print "ex is ", e

        print "over!!!!!!!!!!!!!!!!!"

        return 123

if __name__ == "__main__":
    # try:
    #     v = Vizion('data.panzura.com')

    #     v.create_tenant()
    #     # v.tenant = ('plu+3@panzura.com', 'Sigma@123')
    #     # v.tenant = ('pandas+1605665536@panzura.com', 'QWER123!@#123abc')

    #     v.enable_ASE()
    #     v.create_ASE()

    #     print v.elastic_stack

    #     data = v.get_ASE_stacks_info('vzzc469dpl2su5bpbf')
    #     print data
    #     # v.get_ASE_stacks_info('vzx6ki1fb2ziyrn14t')

    # except Exception as e:
    #     logger.error(e)

    # logger.info("OVER!!!")


    # TODO, 修改一下log等级，Vizion的方法打印出来，page，api那些，等级低点。


    # ---------------------------------------------------

    # compress_png(r"C:\Users\user\Downloads\10.180.206.188_18080_blue_organizations_jenkins_easy_p1_detail_easy_p1_35_pipeline.png")
    compress_png(r"C:\Users\user\Desktop\Snipaste_2021-02-21_14-51-31.png")