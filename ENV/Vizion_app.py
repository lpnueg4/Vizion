# coding: u8

from collections import OrderedDict
import re
import requests
from texttable import Texttable
import time
import traceback
import urllib

from Vizion.ENV.Vizion_base import Vizion as baseVizion

#
from log import logger, log_to_file

class Tenant(object):

    def __init__(self, vizion, email=None, password=None):
        self.vizion = vizion

        self.email = email
        self.password = password

        self.id = None
        self.name = None
        self.company = None

        #
        self._exist = None

        if self.exist:
            logger.info("Tenant-gke::__init__, '%s' exist is %s" % (self.email, self.exist))
        else:
            logger.error("Tenant-gke::__init__, '%s' exist is %s" % (self.email, self.exist))

        self.show()

    @property
    def exist(self):
        if self._exist == None:
            ret = self.vizion.api_auth.api_tenants_get({'q': self.email})
            if len(ret) == 0:
                self._exist = False
            else:
                for k,v in ret[0].iteritems():
                    setattr(self, k, v)
                self._exist = True

        return self._exist

    def show(self):
        table = Texttable()
        table.set_cols_width([40,20,15,15,8,8,25])

        table.add_row(['Email', 'password', 'name', 'company', 'id', 'status', 'created_at'])
        table.add_row([self.email, self.password, self.name, self.company, self.id, self.status, self.created_at])

        logger.info("\n" + table.draw())


class Elastic_Stack(list):

    def __init__(self):
        pass

    def add(self, kibana_url, kibana_username, kibana_password, kibana_api_url):
        if all([kibana_url, kibana_username, kibana_password, kibana_api_url]):
            data = {
                'kibana_url': kibana_url,
                'kibana_username': kibana_username,
                'kibana_password': kibana_password,
                'kibana_api_url': kibana_api_url,
            }
            self.append(data)
        else:
            logger.error("Elastic_Stack::add, invalid parameters")
            logger.error("\nkibana_url: %s\nkibana_username: %s\nkibana_password: %s\nkibana_api_url: %s" % \
                (kibana_url, kibana_username, kibana_password, kibana_api_url))

    def show(self):
        table = Texttable()
        table.set_cols_width([40,20,20,80])
        table.add_row(['Kibana', 'Username', 'Password', 'Elasticsearch API Endpoint'])
        for i in self:
            table.add_row([i['kibana_url'], i['kibana_username'], i['kibana_password'], i['kibana_api_url']])

        logger.info("\n" + table.draw())

#
class Vizion(baseVizion):

    def __init__(self, host):
        self.host = host
        self.timestamp = str(int(time.time()))

        super(Vizion, self).__init__(host)

        print "im gke", self.host

    #-----------------------------------------------------------------------
    @property
    def tenant(self):
        return self._tenant
        # a = super(Vizion,self).tenant
        # return a

    @tenant.setter
    def tenant(self, value):
        email, password = value
        self._tenant = Tenant(self, email, password)
        # a = super(Vizion,self).tenant
        # self._tenant = a(self, email, password)

    #-----------------------------------------------------------------------
    @property
    def api_auth(self):
        if hasattr(self, '_api_auth') and isinstance(self._api_auth, API_auth):
            pass
        else:
            self._api_auth = API_auth(self)

        return self._api_auth

    @api_auth.setter
    def api_auth(self):
        pass

    #-----------------------------------------------------------------------
    @property
    def api_tenantsvr(self):
        if hasattr(self, '_api_tenantsvr') and isinstance(self._api_tenantsvr, API_tenantsvr):
            pass
        else:
            self._api_tenantsvr = API_tenantsvr(self)

        return self._api_tenantsvr

    @api_tenantsvr.setter
    def api_tenantsvr(self):
        pass

    #-----------------------------------------------------------------------

    @property
    def webdriver(self):
        if hasattr(self, '_webdriver') and self._webdriver.service.process != None:
            pass
        else:
            self._webdriver = webdriver.Chrome()
            logger.info("Vizion::webdriver, init done, %s" % self._webdriver.session_id)
        return self._webdriver

    @webdriver.setter
    def webdriver(self):
        pass

    #-----------------------------------------------------------------------

    @property
    def elastic_stack(self):
        if hasattr(self, '_elastic_stack') and isinstance(self._elastic_stack, list):
            pass
        else:
            self._elastic_stack = Elastic_Stack()
        return self._elastic_stack

    @elastic_stack.setter
    def elastic_stack(self):
        pass

    #-----------------------------------------------------------------------


    # def get_tenant(self, username, password):
    #     t = Tenant(self, username, password)
    #     return t

    def re_login(self):
        logger.info("-"*120)
        logger.info("Vizion::re_login, start")
        try:
            if hasattr(self, '_webdriver') and self._webdriver.service.process != None:
                self._webdriver.quit()

            login_page = Login_Page(self)
            login_page.login()

            self.window_verify_email_flag = False

        except Exception as e:
            logger.error(e)
            logger.error("Vizion::re_login, failed")
            raise Exception()

        logger.info("Vizion::re_login, done")

    def _generate_random_email(self, base_email='pandas@panzura.com'):
        try:
            g = re.search(r'(.*?)(@.*)', base_email)
            if g:
                name = g.group(1)
                domain = g.group(2)
                new_email = "%s+%s%s" % (name, self.timestamp, domain)

                return new_email
        except Exception as e:
            logger.error(e)

    def _generate_random_elk_name(self, base_name='auto_test_elk'):
        try:
            new_name = "%s_%s" % (base_name, self.timestamp)
            return new_name
        except Exception as e:
            logger.error(e)

    def create_tenant(self, email=None, password="QWER123!@#123abc"):
        login_page = Login_Page(self)

        if email == None:
            email = self._generate_random_email()

        login_page.create_new_one(email, password)

        time.sleep(10)
        self.tenant = (email, password)
        logger.info("Vizion::create_tenant, done")

        self.webdriver.quit()

    def enable_ASE(self):
        logger.info("Vizion::enable_ASE, start")

        # use api
        original_data = self.api_tenantsvr.sys_tenant_get(self.tenant.name)
        original_data['ALLOWED_SERVICES'] = "ASF,ASE"

        ret = self.api_tenantsvr.sys_tenant_put(self.tenant.name, original_data)
        if ret['message'].lower() == 'success':
            logger.info("Vizion::enable_ASE, done")
        else:
            logger.error("Vizion::enable_ASE, failed")

    def create_ASE(self, name=None, version='6.5.4', retention_period='Forever'):
        logger.info("Vizion::create_ASE, start")

        if name == None:
            name = self._generate_random_elk_name()

        try:
            self.re_login()

            header = Header_in_page(self)
            header.go_to_marketplace()

            marketplace = Marketplace(self)
            marketplace.add_ASE_services(name, version, retention_period)

            ase_page = ASE_page(self)
            ase_page.generate_stack(name, version, retention_period)

            self.elastic_stack.show()

        except Exception as e:
            logger.error(e)
            traceback.print_exc()
            logger.error("Vizion::create_ASE, failed")
            raise Exception()

        logger.info("Vizion::create_ASE, done")

#-------------------------------------------------------

if __name__ == "__main__":
    try:
        v = Vizion('data.panzura.com')

        # v.create_tenant()
        v.tenant = ('plu+3@panzura.com', 'Sigma@123')

        v.enable_ASE()
        v.create_ASE()

    except Exception as e:
        logger.error(e)

    logger.info("OVER!!!")

