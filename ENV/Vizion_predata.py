# coding: u8

from collections import OrderedDict
import re
import requests
from texttable import Texttable
import time
import traceback
import urllib

#
from Vizion.ENV.Vizion_base import Vizion as baseVizion


class Tenant(object):

    def __init__(self, vizion, email=None, password=None):
        self.vizion = vizion

        self.email = email
        self.password = password

        self.id = None
        self.name = None
        self.company = None

        #
        self._exist = None

        if self.exist:
            logger.info("Tenant-gke::__init__, '%s' exist is %s" % (self.email, self.exist))
        else:
            logger.error("Tenant-gke::__init__, '%s' exist is %s" % (self.email, self.exist))

        self.show()

    @property
    def exist(self):
        if self._exist == None:
            ret = self.vizion.api_auth.api_tenants_get({'q': self.email})
            if len(ret) == 0:
                self._exist = False
            else:
                for k,v in ret[0].iteritems():
                    setattr(self, k, v)
                self._exist = True

        return self._exist

    def show(self):
        table = Texttable()
        table.set_cols_width([40,20,15,15,8,8,25])

        table.add_row(['Email', 'password', 'name', 'company', 'id', 'status', 'created_at'])
        table.add_row([self.email, self.password, self.name, self.company, self.id, self.status, self.created_at])

        logger.info("\n" + table.draw())


class Elastic_Stack(list):

    def __init__(self):
        pass

    def add(self, kibana_url, kibana_username, kibana_password, kibana_api_url):
        if all([kibana_url, kibana_username, kibana_password, kibana_api_url]):
            data = {
                'kibana_url': kibana_url,
                'kibana_username': kibana_username,
                'kibana_password': kibana_password,
                'kibana_api_url': kibana_api_url,
            }
            self.append(data)
        else:
            logger.error("Elastic_Stack::add, invalid parameters")
            logger.error("\nkibana_url: %s\nkibana_username: %s\nkibana_password: %s\nkibana_api_url: %s" % \
                (kibana_url, kibana_username, kibana_password, kibana_api_url))

    def show(self):
        table = Texttable()
        table.set_cols_width([40,20,20,80])
        table.add_row(['Kibana', 'Username', 'Password', 'Elasticsearch API Endpoint'])
        for i in self:
            table.add_row([i['kibana_url'], i['kibana_username'], i['kibana_password'], i['kibana_api_url']])

        logger.info("\n" + table.draw())

#
class Vizion(baseVizion):

    def __init__(self, host, robot=False, work_space="c:/tmp"):
        self.host = host
        self.timestamp = str(int(time.time()))

        super(Vizion, self).__init__(host, robot, work_space)

        # self.c3_pw = "Pan1011Viz!dps"
        # self.c3_pw = "Pan1011Viz!ei"
        self.c3_pw = "0&fU#PXqOk9KwdqTtiLJuZ"

#-------------------------------------------------------

if __name__ == "__main__":
    try:
        v = Vizion('data.panzura.com')

        # v.create_tenant()
        v.tenant = ('plu+3@panzura.com', 'Sigma@123')

        v.enable_ASE()
        v.create_ASE()

    except Exception as e:
        logger.error(e)

    logger.info("OVER!!!")

