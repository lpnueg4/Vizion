
import sys
sys.path.insert(0, "../")
sys.path.insert(0, "../../")

# print sys.path

from version_chooser import version_chooser
from Vizion.API.FSS.fss_predata import API_fss
from Vizion.API.VCC.vcc_predata import API_vcc

c = version_chooser("predata.panzura.com", from_robot=0)
# c = version_chooser("data.panzura.com", from_robot=0)
v = c.get_vizion()
# v.tenant = ("ccjenkinstest+1608108613@gmail.com", "QWER123!@#123abc")
# v.tenant = ("ccjenkinstest+1608173677@gmail.com", "QWER123!@#123abc")
# v.tenant = ("ccjenkinstest+1608197737@gmail.com", "QWER123!@#123abc")
# v.tenant = ("ccjenkinstest+1608278266@gmail.com", "QWER123!@#123abc")
# v.tenant = ("ccjenkinstest+1609912448@gmail.com", "QWER123!@#123abc")
# v.tenant = ("ccjenkinstest+1610084277@gmail.com", "QWER123!@#123abc")
v.tenant = ("ccjenkinstest+10086@gmail.com", "QWER123!@#123abc")

#-------------------------------------------------------------

api_vcc_t = API_vcc(v, use_tenant=True)
cca_data = api_vcc_t.application_cca_get()

ccid_list = map(lambda x: x['id'], cca_data['data'])
print ccid_list

#-------------------------------------------------------------

api_fss = API_fss(v)

upgrade_admin_post_data = {
    "bucket": "vizion-metadata-default",
    "cc_version": [
        "7.1.9.6",
        "7.2.0.0",
        "8.0.0.4",
        "20.0.0.0",
        "7.1.9.0",
        "7.2.2.0",
        "8.0.0.1",
        "8.0.0.10",
        "8.0.0.11",
        "7.2.1.0",
        "8.0.0.0"
    ],
    "ccids": ccid_list,
    "filename": "CCapp-1.0.00889.tar.gz",
    "info": "string",
    "path": "application/cca",
    "support_type": 0,
    "type": 2,
    "version": "1.0.00889"
}

api_fss.upgrade_admin_post(upgrade_admin_post_data)

