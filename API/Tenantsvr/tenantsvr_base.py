# coding: u8

import json
import requests

class API_tenantsvr(object):
    # https://data.panzura.com/tenantsvr/swagger/index.html

    def __init__(self, vizion):
        global logger
        logger = vizion.logger

        self.host = vizion.host
        self.headers = vizion.api_auth.headers

        logger.info("API_tenantsvr::__init__, done")

    def sys_tenant_get(self, tenant_name):
        url = "https://%s/tenantsvr/sys_tenant/%s" % (self.host, tenant_name)
        r = requests.get(url, headers=self.headers)

        if r.status_code == 200:
            data = r.json()
            logger.debug(data)
            return data
        else:
            logger.error(r.text)
            raise Exception("API_tenantsvr::sys_tenant_get, failed")

    def sys_tenant_put(self, tenant_name, settings):
        url = "https://%s/tenantsvr/sys_tenant/%s" % (self.host, tenant_name)
        r = requests.put(url, json=settings, headers=self.headers)

        if r.status_code == 200:
            data = r.json()
            logger.debug(data)
            logger.info("API_tenantsvr::sys_tenant_put, done")
            return data
        else:
            logger.error(r.text)
            raise Exception("API_tenantsvr::sys_tenant_put, failed")

