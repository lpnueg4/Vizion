# coding: u8

import json
import requests
import urllib

class API_auth(object):
    # https://data.panzura.com/auth/swagger/index.html
    # https://predata.panzura.com/vcc/swagger
    # https://predata.panzura.com/tenantsvr/swagger/index.html

    def __init__(self, vizion):
        global logger
        logger = vizion.logger

        self.host = vizion.host
        self.email = vizion.c3_email
        self.password = vizion.c3_pw

        self.headers = {"accept": "application/json", "content-type": "application/json"}

        self._login()

        logger.info("API_auth::__init__, done")

    def _login(self):
        try:
            login_url = "https://%s/auth/token/basic_auth" % self.host

            r = requests.post(login_url, json={"username": self.email, "password": self.password}, headers=self.headers)
            if r.status_code == 200:
                data = r.json()
                # self.token = data['id_token']
                self.headers['Authorization'] = "Bearer " + data['id_token']
            else:
                raise Exception("%s: %s" % (r.status_code, r.text))
        except Exception as e:
            logger.error(e)
            logger.error("API_auth::_login, failed, %s / %s" % (self.email, self.password))
            raise Exception()

        logger.info("API_auth::_login, done")

    def api_tenants_get(self, query_str):
        query_str = urllib.urlencode(query_str)
        url = "https://%s/auth/api/tenants?%s&expz=false&page=1&size=100&hasapp=1" % (self.host, query_str)

        r = requests.get(url, headers=self.headers)
        if r.status_code == 200:
            data = r.json()
            logger.debug(data)
            return data
        else:
            logger.error(r.text)
            raise Exception("API_auth::api_tenants_get, failed")

