# coding: u8

import json
import requests
import urllib

from auth_base import API_auth as API_auth_base

class API_auth(API_auth_base):
    # https://data.panzura.com/auth/swagger/index.html

    def __init__(self, vizion):
        global logger
        logger = vizion.logger

        super(API_auth,self).__init__(vizion)
        logger.info("API_auth-gke::__init__, done")

    def _login(self):
        try:
            login_url = "https://%s/auth/token/basic_auth" % self.host

            r = requests.post(login_url, json={"username": self.email, "password": self.password}, headers=self.headers)
            if r.status_code == 200:
                data = r.json()
                # self.token = data['id_token']
                self.headers['Authorization'] = "Bearer " + data['id_token']
            else:
                raise Exception("%s: %s" % (r.code, r.text))
        except Exception as e:
            logger.error(e)
            logger.error("API_auth-gke::_login, failed")
            raise Exception()

        logger.info("API_auth-gke::_login, done")

