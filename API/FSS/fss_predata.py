# coding: u8

import json
import requests
import urllib

class API_fss(object):
    # https://data.panzura.com/auth/swagger/index.html
    # https://predata.panzura.com/vcc/swagger
    # https://predata.panzura.com/tenantsvr/swagger/index.html

    def __init__(self, vizion):
        global logger
        logger = vizion.logger

        self.host = vizion.host
        self.email = vizion.c3_email
        self.password = vizion.c3_pw

        self.headers = {"accept": "application/json", "content-type": "application/json"}

        self._login()

        logger.info("API_fss::__init__, done")

    def _login(self):
        try:
            login_url = "https://%s/auth/token/basic_auth" % self.host

            r = requests.post(login_url, json={"username": self.email, "password": self.password}, headers=self.headers)
            if r.status_code == 200:
                data = r.json()
                # self.token = data['id_token']
                self.headers['Authorization'] = "Bearer " + data['id_token']
            else:
                raise Exception("%s: %s" % (r.status_code, r.text))
        except Exception as e:
            logger.error(e)
            logger.error("API_fss::_login, failed, %s / %s" % (self.email, self.password))
            raise Exception()

        logger.info("API_fss::_login, done")

    def upgrade_admin_post(self, data):
        try:
            url = "https://%s/fss/upgrade/admin/packages" % self.host
            r  = requests.post(url, json=data, headers=self.headers)
            if r.status_code == 200:
                data = r.json()
                logger.debug(data)
                logger.info("API_fss::upgrade_admin_post, done")
                return data
            else:
                logger.error(r.text)
        except Exception as e:
            logger.error(e)
            logger.error("API_fss::upgrade_admin_post, failed")
            raise Exception()

