# coding: u8

import json
import requests
import urllib

class API_vcc(object):
    # https://data.panzura.com/auth/swagger/index.html
    # https://predata.panzura.com/vcc/swagger
    # https://predata.panzura.com/tenantsvr/swagger/index.html

    def __init__(self, vizion, use_tenant=None):
        global logger
        logger = vizion.logger

        self.vizion = vizion
        self.host = vizion.host
        if use_tenant:
            self.email = vizion.tenant.email
            self.password = vizion.tenant.password
            logger.debug("API_vcc::__init__, use tenant login")
        else:
            self.email = vizion.c3_email
            self.password = vizion.c3_pw
            logger.debug("API_vcc::__init__, use c3admin login")

        self.headers = {"accept": "application/json", "content-type": "application/json"}

        self._login()

        logger.info("API_vcc::__init__, done")

    def _login(self):
        try:
            login_url = "https://%s/auth/token/basic_auth" % self.host

            r = requests.post(login_url, json={"username": self.email, "password": self.password}, headers=self.headers)
            if r.status_code == 200:
                data = r.json()
                # self.token = data['id_token']
                self.headers['Authorization'] = "Bearer " + data['id_token']
            else:
                raise Exception("%s: %s" % (r.status_code, r.text))
        except Exception as e:
            logger.error(e)
            logger.error("API_vcc::_login, failed, %s / %s" % (self.email, self.password))
            raise Exception()

        logger.info("API_vcc::_login, done, %s / %s" % (self.email, self.password))

    # need login with tenant
    def application_cca_get(self):
        try:
            tenant_id = self.vizion.tenant.name

            url = "https://%s/vcc/applications/cca?tenant=%s" % (self.host, tenant_id)

            r = requests.get(url, headers=self.headers)
            if r.status_code == 200:
                data = r.json()
                # TODO，写一个方法，处理太长的返回值。
                logger.debug(data)
                logger.info("API_vcc::application_cca_get, done")
                return data
            else:
                logger.error(r.text)

        except Exception as e:
            logger.error(e)
            logger.error("API_vcc::application_cca_get, failed")
            raise Exception()

