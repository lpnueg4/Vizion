# coding: u8

import importlib
import inspect
import os
import traceback

# from Vizion.ENV.Vizion_base import Vizion

from Vizion.log import zlogger, log_to_file
from Vizion.log import robot_logger

class version_chooser(object):

    def __init__(self, host=None, work_space=None, from_robot=None):
        # 检查调用者
        previous_frame = inspect.currentframe().f_back
        file = previous_frame.f_code.co_filename
        file_name = os.path.basename(file)

        self.host = host
        self.work_space = work_space
        if from_robot:
            self.robot = True
        else:
            self.robot = False

        self.logger = self.init_logger(file_name)
        global logger
        logger = self.logger

    def init_logger(self, file_name):
        # 检查调用者
        logger = None
        if file_name == "librarykeywordrunner.py" or file_name == "testlibraries.py":
            logger = robot_logger()
            logger.info("version_chooser::init_logger, call from robot")
        else:
            logger = zlogger
            logger.info("version_chooser::init_logger, call from other")

        return logger

    # @classmethod
    def get_vizion(self):
        name_list = self.host.split('.')
        vizion_version = name_list[0]
        logger.info("version_chooser::get_vizion, start, host is %s" % self.host)

        try:
            auth_moudle = importlib.import_module("Vizion.ENV.Vizion_%s" % vizion_version)
            logger.info("version_chooser::get_vizion, use vizion module %s" % vizion_version)
        except ImportError as e:
            logger.warning("version_chooser::get_vizion, not vizion module for %s, use base" % vizion_version)

            vizion_version = 'base'
            auth_moudle = importlib.import_module("Vizion.ENV.Vizion_%s" % vizion_version)

        except Exception as e:
            logger.error(e)
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        v = auth_moudle.Vizion(self.host, robot=self.robot, work_space=self.work_space)

        return v

# TODO, 看看怎么从robot传递参数，选择版本。
# 现在只能从 host 参数中，选择Vizion的版本，其他模块的版本还没法选择。
if __name__ == "__main__":
    c = version_chooser("predata.panzura.com")
    v = c.get_vizion()
    # print v

    v.tenant = ('pandas+1604911799@panzura.com', 'QWER123!@#123abc')
    # v.enable_ASE()
