# coding: u8

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import *

import time
import traceback

class Header_in_page(object):

    retry_times = 1

    def __init__(self, vizion):
        global logger
        logger = vizion.logger

        self.vizion = vizion
        self.webdriver = vizion.webdriver

        if not hasattr(self.vizion, 'window_verify_email_flag') or self.vizion.window_verify_email_flag == False:
            self._hendle_window_verify_email()

        self._go_to_this_page()

    def _hendle_window_verify_email(self, action='Verify Later'):
        '''
            action: Verify Later / Resend Email
        '''
        try:
            xpath_check = "//div/h2[text()='Thanks for Signing Up!']"
            WebDriverWait(self.webdriver,20).until(EC.visibility_of_element_located((By.XPATH, xpath_check)))
        except TimeoutException as e:
            logger.info("Header_in_page::_hendle_window_verify_email, not found the window")
            return True

        try:
            xpath_verify_later = "//button[text()='Verify Later']"
            xpath_resend_email = "//button[text()='Resend Email']"
            if action.lower() == "Verify Later".lower():
                WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_verify_later))).click()
                self.vizion.window_verify_email_flag = True
            elif action.lower() == "Resend Email".lower():
                WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_resend_email))).click()

        except TimeoutException as e:
            self.vizion.screenshot()
            logger.warning(e)
            logger.warning("Header_in_page::_hendle_window_verify_email, failed, wait element timeout")
            # logger.warning("\n" + traceback.format_exc())
            # raise Exception()
            return False

        except Exception as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.info("Header_in_page::_hendle_window_verify_email, failed")
            raise Exception()

        logger.info("Header_in_page::_hendle_window_verify_email, done")

    def _go_to_this_page(self):
        '''
            1，目的是，来到当前这个类能执行操作的页面
            2，先是，检查页面特征
                2.1，True，结束
                2.2，False，在限制次数内，re_login，进入初始化流程
            3，其他页面，会通过header来进入初始化流程，每个页面都有header。header可以去到每个主要页面。
                如果有其他情况，就直接re_login
        '''
        logger.info("Header_in_page::_go_to_this_page, start")
        is_this_page = False
        try:
            xpath_check = "//header//img[@alt='Home page']"
            WebDriverWait(self.webdriver,30).until(EC.visibility_of_element_located((By.XPATH, xpath_check)))
            is_this_page = True

        except TimeoutException as e:
            self.vizion.screenshot()
            logger.warning(e)
            logger.warning("Header_in_page::_go_to_this_page, failed, wait element timeout")
            # logger.warning("\n" + traceback.format_exc())
            # raise Exception()

        except Exception as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("Header_in_page::_go_to_this_page, failed")
            # raise Exception() # 不抛异常，因为还需要重试。

        if not is_this_page and self.retry_times != 0:
            while self.retry_times:
                self.retry_times -= 1
                self.vizion.re_login()
                self.__init__(self.vizion)
        elif not is_this_page: # 没有重试次数的情况
            self.vizion.screenshot()
            logger.error("Header_in_page::_go_to_this_page, failed without retry")
            raise Exception()

        logger.info("Header_in_page::_go_to_this_page, done")

    def go_to_marketplace(self):
        logger.info("Header_in_page::go_to_marketplace, start")
        try:
            # xpath_check = "//div[./aside/div/input[@placeholder]]//div[text()='File Analytics']"
            # WebDriverWait(self.webdriver,120).until(EC.visibility_of_element_located((By.XPATH, xpath_check)))

            xpath_marketplace = "//div[text()='Marketplace']"
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_marketplace))).click()

            WebDriverWait(self.webdriver,60).until(lambda d: self.webdriver.execute_script('return document.readyState') == 'complete')

        except TimeoutException as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("Header_in_page::go_to_marketplace, failed, wait element timeout")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        except Exception as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("Header_in_page::go_to_marketplace, failed")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        logger.info("Header_in_page::go_to_marketplace, done")

    def go_to_ASE(self):
        logger.info("Header_in_page::go_to_ASE, start")
        try:
            xpath_services = "//div[text()='My Services']"
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_services))).click()

            xpath_ase = "//header//div[contains(text(),'Elasticsearch')]"
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_ase))).click()

            WebDriverWait(self.webdriver,60).until(lambda d: self.webdriver.execute_script('return document.readyState') == 'complete')

        except TimeoutException as e:
            self.vizion.screenshot()
            logger.warning(e)
            logger.warning("Header_in_page::go_to_ASE, failed, wait element timeout")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        except Exception as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("Header_in_page::go_to_ASE, failed")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        logger.info("Header_in_page::go_to_ASE, done")