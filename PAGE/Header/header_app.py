# coding: u8

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import *

import traceback

#
from Vizion.PAGE.Header.header_base import Header_in_page as baseHeader

class Header_in_page(baseHeader):

    retry_times = 1

    def __init__(self, vizion):
        global logger
        logger = vizion.logger

        super(Header_in_page, self).__init__()

    def _hendle_window_verify_email(self, action='Verify Later'):
        '''
            action: Verify Later / Resend Email
        '''
        try:
            xpath_check = "//div/h2[text()='Thanks for signing up!']"
            WebDriverWait(self.webdriver,60).until(EC.visibility_of_element_located((By.XPATH, xpath_check)))
        except TimeoutException as e:
            logger.info("Header_in_page-app::_hendle_window_verify_email, not found the window")
            return True

        try:
            xpath_verify_later = "//button[text()='Verify Later']"
            xpath_resend_email = "//button[text()='Resend Email']"
            if action.lower() == "Verify Later".lower():
                WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_verify_later))).click()
                self.vizion.window_verify_email_flag = True
            elif action.lower() == "Resend Email".lower():
                WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_resend_email))).click()

        except TimeoutException as e:
            logger.error(e)
            logger.error("Header_in_page-app::_hendle_window_verify_email, failed, wait element timeout")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        except Exception as e:
            logger.error(e)
            logger.info("Header_in_page-app::_hendle_window_verify_email, failed")

        logger.info("Header_in_page-app::_hendle_window_verify_email, done")

