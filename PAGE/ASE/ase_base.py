# coding: u8

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import *

import time
import traceback

# TODO, 这里要选择版本怎么办？
# 是不是应该每个对象，都设计一个版本选择器。
from Vizion.PAGE.Header.header_base import Header_in_page

class ASE_page(object):

    retry_times = 1

    def __init__(self, vizion):
        global logger
        logger = vizion.logger

        self.vizion = vizion
        self.webdriver = vizion.webdriver

        self._hendle_window_welcome()
        self._go_to_this_page()

    def _hendle_window_welcome(self):
        try:
            xpath_check = "//a[text()='Next']"
            WebDriverWait(self.webdriver,20).until(EC.visibility_of_element_located((By.XPATH, xpath_check)))
        except TimeoutException as e:
            logger.info("ASE_page::_hendle_window_welcome, not found the window")
            return True

        try:
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_check))).click()
        except TimeoutException as e:
            self.vizion.screenshot()
            logger.warning(e)
            logger.warning("ASE_page::_hendle_window_welcome, failed, wait element timeout")
            # logger.warning("\n" + traceback.format_exc())
            # raise Exception()
            return False

        except Exception as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.info("ASE_page::_hendle_window_welcome, failed")
            raise Exception()

        logger.info("ASE_page::_hendle_window_welcome, done")

    def _go_to_this_page(self):
        logger.info("ASE_page::_go_to_this_page, start")
        is_this_page = False
        try:
            xpath_check = "//div[./aside//*[name()='svg' and @direction='left']]//div[text()='Dashboard']"
            WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_check)))

            xpath_check = "//div[./aside//*[name()='svg' and @direction='left']]//div[text()='Manage Stacks']"
            WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_check)))

            is_this_page = True

        except TimeoutException as e:
            self.vizion.screenshot()
            logger.warning(e)
            logger.warning("ASE_page::_go_to_this_page, failed, wait element timeout")
            # logger.warning("\n" + traceback.format_exc())
            # raise Exception()

        except Exception as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("ASE_page::_go_to_this_page, failed")
            logger.critical("\n" + traceback.format_exc())
            # raise Exception() # 不抛异常，因为还需要重试。

        if not is_this_page and self.retry_times != 0:
            # self.vizion.re_login() # header里面会re_login
            while self.retry_times:
                self.retry_times -= 1
                header = Header_in_page(self.vizion)
                header.go_to_ASE()
                self.__init__(self.vizion)
        elif not is_this_page: # 没有重试次数的情况
            self.vizion.screenshot()
            logger.error("ASE_page::_go_to_this_page, failed without retry")
            raise Exception()

        logger.info("ASE_page::_go_to_this_page, done")

    def generate_stack(self, name, version, retention_period):
        logger.info("ASE_page::generate_stack, start\n\tname: %s, version: %s, retention_period: %s" % (name, version, retention_period))
        try:
            # p1
            xpath_manage_stacks = "//div[./aside//*[name()='svg' and @direction='left']]//div[text()='Manage Stacks']"
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_manage_stacks))).click()

            xpath_generate_stack = "//button[text()='Generate Stack']"
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_generate_stack))).click()

            # p2
            xpath_check = "//div/h1[text()='Generate Stack']"
            WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_check)))

            xpath_stack_name = "//input[@id='stack-name']"
            a = WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_stack_name)))
            a.clear()
            a.send_keys(name)

            xpath_retention_period_arrow = "//div[./div/label[text()='Select the retention period']]//*[name()='svg' and @width]"
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_retention_period_arrow))).click()

            xpath_retention_period_item = "//div[./div/label[text()='Select the retention period']]//div[@data-placement='bottom-start']//div[text()='%s']" % retention_period
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_retention_period_item))).click()

            xpath_version = "//div[./h3[text()='Elastic Stack Version']]//div[text()='Elastic Stack %s']" % version
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_version))).click()

            xpath_generate_stack_button = "//button[text()='Generate Stack']"
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_generate_stack_button))).click()

            # p3
            xpath_kibana_url = "//div[./div[contains(text(),'To log into Kibana')]]//pre/a"
            kibana_url = WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_kibana_url))).text

            xpath_kibana_username = "//div[./div[contains(text(),'kibana Username')]]//pre"
            kibana_username = WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_kibana_username))).text

            xpath_kibana_password = "//div[./div[contains(text(),'kibana Password')]]//pre"
            kibana_password = WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_kibana_password))).text

            xpath_kibana_api_url = "//div[./div[contains(text(),'Elasticsearch API')]]//pre/a"
            kibana_api_url = WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_kibana_api_url))).text

            self.vizion.elastic_stack.add(name, version, retention_period, kibana_url, kibana_username, kibana_password, kibana_api_url)

        except TimeoutException as e: # 不要改，就是这样。
            self.vizion.screenshot()
            logger.error(e)
            logger.error("ASE_page::generate_stack, failed, wait element timeout")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        except Exception as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("ASE_page::generate_stack, failed")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        logger.info("ASE_page::generate_stack, done")

    def _go_to_manage_stacks(self):
        logger.info("ASE_page::_go_to_manage_stacks, start")
        try:
            xpath_manage_stacks = "//div[./aside//*[name()='svg' and @direction='left']]//div[text()='Manage Stacks']"
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_manage_stacks))).click()

            WebDriverWait(self.webdriver,60).until(lambda d: self.webdriver.execute_script('return document.readyState') == 'complete')
            time.sleep(1)

        except TimeoutException as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("ASE_page::_go_to_manage_stacks, failed, wait element timeout")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        except Exception as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("ASE_page::_go_to_manage_stacks, failed")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        logger.info("ASE_page::_go_to_manage_stacks, done")

    def get_stacks_info(self, app_id):
        logger.info("ASE_page::get_stacks_info, start\n\tapp_id: %s" % app_id)
        data = {}
        try:
            self._go_to_manage_stacks()

            # ------------------------------------- get all
            # c = self.webdriver.find_elements(By.XPATH, xpath_get_stacks_count)
            # xpath_get_stacks_count = "//div[./div/div[text()='Active Elastic Stacks']]//table/tbody/tr"
            # stacks_list = self.webdriver.find_elements_by_xpath(xpath_get_stacks_count);

            # logger.info("ASE_page::get_stacks_info, here is %s stacks" % len(stacks_list))
            # -------------------------------------

            xpath_table = "//div[./div/div[text()='Active Elastic Stacks']]//table/tbody"
            xpath_elk_name       = xpath_table + "/tr[./td[contains(text(),'%s')]]/td[2]" % app_id
            xpath_elk_id         = xpath_table + "/tr[./td[contains(text(),'%s')]]/td[3]" % app_id
            xpath_bytes_stored   = xpath_table + "/tr[./td[contains(text(),'%s')]]/td[4]" % app_id
            xpath_daily_indexed  = xpath_table + "/tr[./td[contains(text(),'%s')]]/td[5]" % app_id
            xpath_query_count    = xpath_table + "/tr[./td[contains(text(),'%s')]]/td[6]" % app_id
            xpath_document_count = xpath_table + "/tr[./td[contains(text(),'%s')]]/td[7]" % app_id

            data['name'] =           WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_elk_name))).text
            data['id'] =             WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_elk_id))).text
            data['bytes_stored'] =   WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_bytes_stored))).text
            data['daily_indexed'] =  WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_daily_indexed))).text
            data['query_count'] =    WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_query_count))).text
            data['document_count'] = WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_document_count))).text

            for k,v in data.items():
                if v == '':
                    data[k] = 0

        except TimeoutException as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("ASE_page::get_stacks_info, failed, wait element timeout")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        except Exception as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("ASE_page::get_stacks_info, failed")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        logger.info("ASE_page::get_stacks_info, done")

        return data
