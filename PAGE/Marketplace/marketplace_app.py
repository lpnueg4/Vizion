# coding: u8

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import *

import traceback

#
from Vizion.PAGE.Marketplace.marketplace_base import Marketplace_Page as baseMarketplace

def is_element_exist(webdriver, element):
    flag = True
    try:
        webdriver.find_element_by_xpath(element)
    except:
        flag = False
    return flag

class Marketplace_Page(baseMarketplace):

    def __init__(self, vizion):
        global logger
        logger = vizion.logger

        super(Header_in_page, self).__init__()

    def add_ASE_services(self, name, version, retention_period):
        logger.info("Marketplace_Page::add_ASE_services, start\n\tname: %s, version: %s, retention_period: %s" % (name, version, retention_period))
        try:
            # p1
            xpath_check = "//div[text()='Browse All']"
            WebDriverWait(self.webdriver,120).until(EC.visibility_of_element_located((By.XPATH, xpath_check)))

            xpath_create_ase = "//div[./h1[text()='Vizion Services']]//div[contains(text(),'Elasticsearch')]"
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_create_ase))).click()

            # p2
            xpath_check = "//div[text()='Minimum Requirements']"
            WebDriverWait(self.webdriver,20).until(EC.visibility_of_element_located((By.XPATH, xpath_check)))

            # sometime, already create ASE
            xpath_open_dashboard = "//div[./div[text()='Billed by Plan Size']]/div//button[text()='Open Dashboard']"
            ret = is_element_exist(self.webdriver, xpath_open_dashboard)
            if ret:
                logger.warning("Marketplace_Page::add_ASE_services, already add ASE services")
                return True

            # xpath_start_free_trial = "//div[./div/img[@title='FileBeats_image.png']]//a/button"
            xpath_start_free_trial = "//div[./div[text()='Billed by Plan Size']]/div/button[text()='Start Free Trial']"
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_start_free_trial))).click()

            # p3
            xpath_check = "//div[text()='Choose what fits you best']"
            WebDriverWait(self.webdriver,20).until(EC.visibility_of_element_located((By.XPATH, xpath_check)))

            xpath_generate_stack = "//div/a[text()='Generate Stack']"
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_generate_stack))).click()

        except TimeoutException as e:
            logger.error(e)
            logger.error("Marketplace_Page::add_ASE_services, failed, wait element timeout")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        except Exception as e:
            logger.error(e)
            logger.info("Marketplace_Page::add_ASE_services, failed")

        logger.info("Marketplace_Page::add_ASE_services, done")

