# coding: u8

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import *

import traceback

def is_element_exist(webdriver, element):
    flag = True
    try:
        webdriver.find_element_by_xpath(element)
    except:
        flag = False
    return flag

class Marketplace_Page(object):

    retry_times = 1

    def __init__(self, vizion):
        global logger
        logger = vizion.logger

        self.vizion = vizion
        self.webdriver = vizion.webdriver

        self._go_to_this_page()

    def _go_to_this_page(self):
        logger.info("Marketplace_Page::_go_to_this_page, start")
        is_this_page = False
        try:
            xpath_check = "//div[text()='Browse All']"
            WebDriverWait(self.webdriver,60).until(EC.visibility_of_element_located((By.XPATH, xpath_check)))
            is_this_page = True

        except TimeoutException as e:
            self.vizion.screenshot()
            logger.warning(e)
            logger.warning("Marketplace_Page::_go_to_this_page, failed, wait element timeout")
            # logger.warning("\n" + traceback.format_exc())
            # raise Exception()

        except Exception as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("Marketplace_Page::_go_to_this_page, failed")
            raise Exception()

        if not is_this_page and self.retry_times != 0:
            # self.vizion.re_login() # header里面会re_login
            while self.retry_times:
                self.retry_times -= 1
                header = Header_in_page(self.vizion)
                header.go_to_marketplace()
                self.__init__(self.vizion)
        elif not is_this_page: # 没有重试次数的情况
            self.vizion.screenshot()
            logger.error("Marketplace_Page::_go_to_this_page, failed without retry")
            raise Exception()

        logger.info("Marketplace_Page::_go_to_this_page, done")

    def add_ASE_services(self):
        logger.info("Marketplace_Page::add_ASE_services, start")
        try:
            # p1
            xpath_check = "//div[text()='Browse All']"
            WebDriverWait(self.webdriver,120).until(EC.visibility_of_element_located((By.XPATH, xpath_check)))

            xpath_create_ase = "//div[./h1[text()='Panzura Data Services']]//div[contains(text(),'Elasticsearch')]"
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_create_ase))).click()

            # p2
            xpath_check = "//div[text()='Minimum Requirements']"
            WebDriverWait(self.webdriver,20).until(EC.visibility_of_element_located((By.XPATH, xpath_check)))

            # sometime, already create ASE
            xpath_open_dashboard = "//div[./div[text()='Billed by Plan Size']]/div//button[text()='Open Dashboard']"
            ret = is_element_exist(self.webdriver, xpath_open_dashboard)
            if ret:
                logger.warning("Marketplace_Page::add_ASE_services, already add ASE services")
                return True

            # xpath_start_free_trial = "//div[./div/img[@title='FileBeats_image.png']]//a/button"
            xpath_start_free_trial = "//div[./div[text()='Billed by Plan Size']]/div/button[text()='Start Free Trial']"
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_start_free_trial))).click()

            # p3
            xpath_check = "//div[text()='Choose what fits you best']"
            WebDriverWait(self.webdriver,20).until(EC.visibility_of_element_located((By.XPATH, xpath_check)))

            xpath_generate_stack = "//div/a[text()='Generate Stack']"
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_generate_stack))).click()

        except TimeoutException as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("Marketplace_Page::add_ASE_services, failed, wait element timeout")
            logger.critical("\n" + traceback.format_exc()) # 这种是一个任务，如果找不到元素，意味着任务失败，需要抛出异常，
                                                           # _go_to_this_page，这种是会重试的。第一次可以找不到元素，所以不抛异常。
            raise Exception()

        except Exception as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("Marketplace_Page::add_ASE_services, failed")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        logger.info("Marketplace_Page::add_ASE_services, done")

