# coding: u8

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import *

import time
import traceback

#
from Vizion.PAGE.Header.header_base import Header_in_page

class Login_Page(object):

    def __init__(self, vizion):
        global logger
        logger = vizion.logger

        self.vizion = vizion
        self.webdriver = vizion.webdriver
        self.init()

    def init(self):
        logger.info("Login_Page::init, start")
        try:
            url = "https://%s" % self.vizion.host
            self.webdriver.get(url)
            logger.info("Login_Page::init, open %s" % url)
            # self.webdriver.maximize_window()
        except Exception as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("Login_Page::init, failed")
            raise Exception()

        logger.info("Login_Page::init, done")

    def create_new_one(self, email, password):
        logger.info("Login_Page::create_new_one, start")
        try:
            # p1
            xpath_login_logo = "//img[@alt='Home page']"
            xpath_signup = "//span[text()='Create one here']"

            # WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_login_logo)), message="not found xpath_login_logo")
            WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_login_logo)))
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_signup))).click()

            # p2
            xpath_firstname = "//input[@name='firstName']"
            xpath_lastname = "//input[@name='lastName']"
            xpath_email = "//input[@name='owneremail']"
            xpath_phone = "//input[@name='phone']"
            xpath_password = "//input[@name='password']"
            xpath_confirmpassword = "//input[@name='confirmPassword']"
            xpath_accept = "//div[./div/span[contains(text(),'Please')]]//*[name()='svg']"
            xpath_next = "//button[text()='Next' and not(@disabled)]"

            data = {
                'firstname': 'F_%s' % email[:12],
                'lastname': 'L_%s' % email[:12],
                'email': email,
                'phone': '123456789',
                'password': password,
                'confirmpassword': password,
            }

            a = WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_firstname)))
            a.clear()
            a.send_keys(data['firstname'])

            a = WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_lastname)))
            a.clear()
            a.send_keys(data['lastname'])

            a = WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_email)))
            a.clear()
            a.send_keys(data['email'])

            a = WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_phone)))
            a.clear()
            a.send_keys(data['phone'])

            a = WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_password)))
            a.clear()
            a.send_keys(data['password'])

            a = WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_confirmpassword)))
            a.clear()
            a.send_keys(data['password'])

            time.sleep(2)

            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_accept))).click()
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xpath_next))).click()

        except TimeoutException as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("Login_Page::create_new_one, failed, wait element timeout")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        except Exception as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("Login_Page::create_new_one, failed")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        logger.info("Login_Page::create_new_one, done")

    def login(self):
        logger.info("Login_Page::login, start")
        try:
            # p1
            xpath_login_logo = "//img[@alt='Home page']"
            xpath_username = "//input[@name='username']"
            xpath_password = "//input[@name='password']"
            xparh_login = "//button[text()='Login']"

            WebDriverWait(self.webdriver,120).until(EC.visibility_of_element_located((By.XPATH, xpath_login_logo)))

            logger.debug("Login_Page::login, %s, %s" % (self.vizion.tenant.email, self.vizion.tenant.password))

            # enter email and password
            a = WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_username)))
            a.clear()
            a.send_keys(self.vizion.tenant.email)
            a = WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_password)))
            a.clear()
            a.send_keys(self.vizion.tenant.password)

            # click login
            WebDriverWait(self.webdriver,10).until(EC.element_to_be_clickable((By.XPATH, xparh_login))).click()

            # check
            try:
                xpath_login_failed = "//div[./form]/div[1]//div[contains(text(),'5 failures')]"
                ele = WebDriverWait(self.webdriver,10).until(EC.visibility_of_element_located((By.XPATH, xpath_login_failed)))
                if ele:
                    logger.error("Login_Page::login, failed, incorrect password")
                    raise Exception()
            except Exception as e:
                pass

            WebDriverWait(self.webdriver,120).until(lambda d: self.webdriver.execute_script('return document.readyState') == 'complete')

            # for handle email verify window
            header = Header_in_page(self.vizion)

        except TimeoutException as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("Login_Page::login, failed, wait element timeout")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        except Exception as e:
            self.vizion.screenshot()
            logger.error(e)
            logger.error("Login_Page::login, failed")
            logger.critical("\n" + traceback.format_exc())
            raise Exception()

        logger.info("Login_Page::login, done")

