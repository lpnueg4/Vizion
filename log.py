# coding: u8

import random
import time

import logging, logzero
from logzero import logger as zlogger
from logzero.colors import Fore as ForegroundColors

# init logger
fmt = "%(asctime)s %(color)s%(levelname)-8.8s #%(lineno)-3d : %(message)s%(end_color)s"
datefmt = '%y/%m/%d %H:%M:%S'
colors = {
    logging.DEBUG:      ForegroundColors.CYAN,
    logging.INFO:       ForegroundColors.GREEN,
    logging.WARNING:    ForegroundColors.YELLOW,
    logging.ERROR:      ForegroundColors.LIGHTRED_EX,
    logging.CRITICAL:   ForegroundColors.LIGHTMAGENTA_EX
}
logzero.formatter(logzero.LogFormatter(fmt=fmt, datefmt=datefmt, colors=colors))
logzero.loglevel(logging.DEBUG)

def log_to_file():
    try:
        random_str = "".join(random.sample('zyxwvutsrqponmlkjihgfedcba',5))
        time_str = time.strftime("%Y-%m-%d_%H-%M-%S", time.localtime())
        file_name = "log_%s_%s.log" % (time_str, random_str)
        logzero.logfile(file_name, loglevel=logging.DEBUG, encoding='utf-8')
        zlogger.debug("Logging to %s" % file_name)
    except Exception as e:
        zlogger.error("failed to log to file\nREASON: %s" % e)

#------------------------------------------------------------------------------------------

from robot.output import librarylogger
from robot.running.context import EXECUTION_CONTEXTS

also_console = True

class robot_logger(object):
    def write(self, msg, level='INFO', html=False):

        if EXECUTION_CONTEXTS.current is not None:
            librarylogger.write(msg, level, html)
        else:
            logger = logging.getLogger("RobotFramework")
            level = {'TRACE': logging.DEBUG // 2,
                     'DEBUG': logging.DEBUG,
                     'INFO': logging.INFO,
                     'HTML': logging.INFO,
                     'WARN': logging.WARN,
                     'ERROR': logging.ERROR,
                     'CRITICAL': logging.CRITICAL}[level]
            logger.log(level, msg)


    def trace(self, msg, html=False):
        """Writes the message to the log file using the ``TRACE`` level."""
        self.write(msg, 'TRACE', html)


    def debug(self, msg, html=False):
        """Writes the message to the log file using the ``DEBUG`` level."""
        self.write(msg, 'DEBUG', html)


    # def info(self, msg, html=False, also_console=False):
    def info(self, msg, html=False):
        """Writes the message to the log file using the ``INFO`` level.

        If ``also_console`` argument is set to ``True``, the message is
        written both to the log file and to the console.
        """
        self.write(msg, 'INFO', html)
        if also_console:
            self.console(msg)


    def warn(self, msg, html=False):
        """Writes the message to the log file using the ``WARN`` level."""
        self.write(msg, 'WARN', html)

    def warning(self, msg, html=False):
        """Writes the message to the log file using the ``WARN`` level."""
        self.write(msg, 'WARN', html)


    def error(self, msg, html=False):
        """Writes the message to the log file using the ``ERROR`` level.

        New in Robot Framework 2.9.
        """
        self.write(msg, 'ERROR', html)

    def critical(self, msg, html=False):
        self.write(msg, 'ERROR', html)
        # try:
        #     print "-------------------------------------------"
        #     self.write(msg, 'CRITICAL', html)
        # except Exception as e:
        #     print e


    def console(self, msg, newline=True, stream='stdout'):
        """Writes the message to the console.

        If the ``newline`` argument is ``True``, a newline character is
        automatically added to the message.

        By default the message is written to the standard output stream.
        Using the standard error stream is possibly by giving the ``stream``
        argument value ``'stderr'``.
        """
        librarylogger.console(msg, newline, stream)
